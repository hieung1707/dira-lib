#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int d = 80;
int rows = 160;
int cols = 320;
int square = 7;
int margin_roi = 50;

int max(int a, int b) {
    return (a >= b) ? a : b;
}

int min(int a, int b) {
    return (a <= b) ? a : b;
}

void get_points(int img[rows][cols], float roi, int *x_left, int *x_right, int *y, bool *has_road) {
    int i = rows * roi;
    int border = (square - 1) / 2;
    int i_l = border;
    int i_r = cols -1 - border;
    int white;
    *has_road = false;

    while (i_l < cols - border) {
        white = 0;
        for (int m = i - border; m <= i + border; m++)
            for (int n = i_l - border; n <= i_l + border; n++) {
                white += img[m][n];
            }
        if (white == (int) pow(square, 2)) {
            if (i_l >= border) {
                *has_road = true;
                break;
            }
        }
        i_l += border + 1;
    }

    while (i_r > i_l) {
        white = 0;
        for (int m = i - border; m <= i + border; m++)
            for (int n = i_r - border; n <= i_r + border; n++)
                white += img[m][n];
        if (white == (int) pow(square, 2)) {
            if (i_r <= 320 - border) {
                *has_road = 1;
                break;
            }
        }
        i_r -= (border + 1);
    }
    if (!has_road) {
        *x_left = 0;
        *x_right = 0;
    }
    else {
        *x_left = i_l;
        *x_right = i_r;
    }
    *y = i;
}

void get_center_points_by_roi(int img[rows][cols], float roi, int *x_left, int *x_right, int *y, bool *has_road) {
    float temp_roi = roi;
    while (temp_roi <= 0.95 && *has_road == 0) {
        get_points(img, temp_roi, x_left, x_right, y, has_road);
        temp_roi += 0.05;
    }
}
