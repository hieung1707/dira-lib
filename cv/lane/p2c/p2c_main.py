import numpy as np
import numpy.ctypeslib as npct
from ctypes import *
import ctypes as ct
# import get_point as gp
# import random
# import time


from dira.__utils._config import Config
path = Config().p2c_path
libc = npct.load_library('p2c', path)

rows = 160
cols = 320
array_2d_int = npct.ndpointer(dtype=np.int32, ndim=2, flags='CONTIGUOUS')
libc.get_center_points_by_roi.argtypes = [array_2d_int, c_float, POINTER(c_int), POINTER(c_int), POINTER(c_int), POINTER(c_bool)]


def get_center_points_by_roi(img, roi):
    """
    Get x coordinates of both sidelines of the road by using a horizontal line (ROI) to find intersection points with
    segmented image.

    Parameters
    ----------
    img: ndarray
        2D array segmented image contains only 0s and 1s as values.
    roi: float, range[0, 1]
        Percentage of image height where ROI will be applied.
        The image height and width follows image coordinate axes, where (0, 0) is at the top left of image.
    Returns
    -------
    x left: x coordinate of left side of the road
    x right: x coordinate of right side of the road

    Examples
    -------
    ROI = 0.5 means ROI line is at 50% of image height.

    """
    p_x_left = pointer(c_int())
    p_x_right = pointer(c_int())
    p_y = pointer(c_int())
    p_has_road = pointer(c_bool())
    libc.get_center_points_by_roi(img, roi, p_x_left, p_x_right, p_y, p_has_road)
    if not p_has_road.contents.value:
        return 0, 0
    return p_x_left.contents.value, p_x_right.contents.value
