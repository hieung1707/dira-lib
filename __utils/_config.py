import os


class Config:
    def __init__(self):
        self.path = os.getcwd()
        # Lane segmentation model path
        self.model_lane_path = os.path.join(self.path, '__models', 'lane.pb')
        # Sign classification model path
        self.model_sign_path = os.path.join(self.path, '__models', 'sign.h5')
        # python to C processing path
        self.p2c_path = os.path.join(self.path, '__p2c')
