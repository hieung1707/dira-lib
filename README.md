# DiRa Autonomous Driving #
## Introduction ##
DiRa Autonomous Driving library serves the purpose of solving self driving problems.
The library is used inside the scope of [Digital Racing](https://cuocduaso.fpt.com.vn/en) competition.
## Library Structure ##
The following structure is applied:

```
dira (wrapper)
├── cv (wrapper)
│   ├── lane
│   ├── obstacle
│   ├── sign
│   └── other problems (TBD)
│
├── other methods (TBD)	
│   └── other problems (TBD)
│
├── __models
└── __utils
    └── _config
```
### Algorithms ### 
- Container for methods used to tackle autonomous control problems, such as computer vision, localizations, etc.
- All current and future algorithms is defined by the following folder structure: dira/[\method\]\[[problem\]/\[algorithm name\]
- For the current version, all the problems stated are solved using computer vision, hence it is the only method available
- For the current version, the library provides solutions for the following problems:
	- Lane segmentation
	- Obstacle detection
	- Sign recognition
### Models ###
All deep learning models used in this library is contained in __models directory
### Utils ###
All path related variables are defined in __utils directory
